module github.com/c3d2/telme10

go 1.17

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf
	github.com/spreadspace/telgo v0.0.0-20170609015223-7277b0d8090e
)
